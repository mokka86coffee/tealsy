<?php
//     header('Access-Control-Allow-Origin: *');
    date_default_timezone_set('Europe/Moscow');
//     locale_set_default('ru-RU');
      // логирование
      if (isset($_GET['record']) && !empty($_GET['record'])) {
                $html_start = '<!DOCTYPE html>
                  <html lang="en">
                  <head>
                        <meta charset="UTF-8">
                        <meta name="viewport" content="width=device-width, initial-scale=1.0">
                        <title>Document</title>
                        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
                  </head>
                  <body class="bg-dark p-5">
                        <table class="table table-bordered table-dark">
                              <thead class="thead-dark">
                                    <th class="text-center text-warning px-2">Время</th>
                                    <th class="text-center text-warning px-2">Действие</th>
                              </thead>
                              <tbody>';
                $html_end = '
                              </tbody>
                        </table>
                        </body>
                  </html>
            ';

                echo $html_start;


                $filename = date('Y') . ".txt";
                $file_stream = fopen($filename, "r") or die("Unable to open file to read! " . $filename);

                while (!feof($file_stream)) {
                    $line = fgets($file_stream);

                    if ($line) {
                        $parts = explode("||", $line);
                        echo "
                              <tr>
                                    <td class='h5'>$parts[0]</td>
                                    <td>$parts[1]</td>
                              </tr>";
                    }
                }

                fclose($file_stream);

                echo $html_end;
                die();
            }

      function logger ($record = 'record') {
            $record = date('l jS \of F Y h:i:s A') . "  ||  $record\n";
            $filename = date('Y') . ".txt";
            file_put_contents($filename, $record, FILE_APPEND | LOCK_EX);
            //  or die("Unable to open file to write! " . $filename);;
            // $file_stream = fopen($filename, "a");
            // fwrite($file_stream, $record);
            // fclose($file_stream);
      }
      // логирование


    $time_intervals = ['08:00','08:30','09:00','09:30','10:00','10:30','11:00','11:30','12:00','12:30','13:00','13:30','14:00','14:30','15:00','15:30','16:00','16:30','17:00','17:30','18:00','18:30','19:00','19:00','19:30','20:00','20:30','21:00','21:30','22:00'];

      function getIP() {
            if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
                  $ip = $_SERVER['HTTP_CLIENT_IP'];
            } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
                  $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
            } else {
                  $ip = $_SERVER['REMOTE_ADDR'];
            }
            return $ip;
      }

    //--------------- Выполнение процедур
    function sql_run($sql, $params = array()) {

        //--------------- Подключение к базе

        $host = '10.0.0.40';

        $connectionOptions = array(
            'Database'     => 'Loyalty',
            'Uid'          => 'Conference',
            'PWD'          => 'M4931e56',
            'CharacterSet' => 'UTF-8'
        );

        $conn = sqlsrv_connect($host, $connectionOptions);

        ///--------------- Подготовка запроса
        $query = sqlsrv_prepare($conn, $sql, $params);

        //--------------- Проверка на ошибки при вызове подготовки запроса
        if (!$query) {
            echo 'Проверка на ошибки при вызове подготовки запроса - error<br>';
            print_r(sqlsrv_errors());
            // echo json_encode(sqlsrv_errors());
            die();
        }

        //--------------- Исполнение SQL и проверка на ошибки
        if (sqlsrv_execute($query) === false) {
            echo 'Исполнение SQL и проверка на ошибки - error<br>';
            print_r(sqlsrv_errors());
            // echo json_encode(sqlsrv_errors());
            die();
        }

        $rows = array();

        // данные ответа
        while( $row = sqlsrv_fetch_array( $query, SQLSRV_FETCH_ASSOC )) {
            array_push($rows, $row);
        }

        return $rows;
    }
    //--------------- Выполнение процедур

    //  вопросы
    // получение встречи по id

    // Получение списка переговорок
    function get_conference_rooms($id = null, $floor = null) {
        $sql = "EXEC [srv-sql01].[Loyalty].[dbo].[GetConferenceRoom]"
        . "@id_room = '" . $id . "',\n" // указывается id комнаты, инфа только по ней
        . "@floor='" . $floor . "'\n"; // указывается этаж, отдаются все комнаты этого этажа

        return sql_run($sql, [$id]);
    }

    // Добавление / изменение / удаление брони
    function modify_reservation($data) {
        $sql = "DECLARE	@res int 
            EXEC @res = [srv-sql01].[Loyalty].[dbo].[UpdConferenceReservation]\n" // - запись результата
            . "@id_reservation = " . $data['id_reservation'] . ",\n" // — id назначеной встречи (для редактирования)/ NULL для создания новой конф-ции
            . "@datefrom = '" . $data['datefrom'] . "',\n" // —время начала '2019-10-18T16:30:00.000'
            . "@dateto = '" . $data['dateto'] . "',\n" // — окончание встречи '2019-10-18T17:30:00.000'
            . "@id_room = " . $data['id_room'] . ",\n" // — id переговорки
            . "@id_organizer = '" . $data['id_organizer'] . "',\n" // — id организатора (начало с 0 поэтому строка)
            . "@description = '" . $data['description'] . "',\n" // —описание
            . "@name = '" . $data['name'] . "',\n" // — название встречи
            . "@del = 0 \n" // — для удаления @del = 1 и id_reservation которую нужно удалить"
            . "SELECT 'id_reservation' = @res" . "\n"
        ;

        return sql_run($sql);
    }

    // Добавление / изменение / удаление брони
    function delete_reservation($id_reservation) {
        $sql = "DECLARE	@res int
            EXEC @res = [srv-sql01].[Loyalty].[dbo].[UpdConferenceReservation]\n" // - запись результата
            . "@id_reservation = " . $id_reservation . ",\n" // — id назначеной встречи (для редактирования)/ NULL для создания новой конф-ции
            // . "@datefrom = '2019-10-18T16:30:00.000',\n" // —время начала '2019-10-18T16:30:00.000'
            // . "@dateto = '2019-10-18T16:30:00.000',\n" // — окончание встречи '2019-10-18T17:30:00.000'
            // . "@id_room = 1,\n" // — id переговорки
            // . "@id_organizer = '',\n" // — id организатора (начало с 0 поэтому строка)
            // . "@description = '',\n" // —описание
            // . "@name = '',\n" // — название встречи
            . "@del = 1 \n" // — для удаления @del = 1 и id_reservation которую нужно удалить"
            . "SELECT 'Return Value' = @res" . "\n"
        ;

        return sql_run($sql);
    }

    // Получение списка сотрудников
    function get_participants($id_reservation = null, $fio = null) {
        $sql = "EXEC  [srv-sql01].[Loyalty].[dbo].[GetConferee]"
            . "@id_reservation=?," // — id встречи, если 0, то список всех сотрудников для поиска
            . "@FIO=?" // — id встречи, если 0, то список всех сотрудников для поиска
        ;

        return sql_run($sql, [$id_reservation, $fio]);
    }

    // Добавление / изменение / удаление участников встречи
    function modify_participants($id_reservation, $id_employee, $delete_flag = 0) {
        // echo json_encode([$id_reservation, $id_employee, $delete_flag]);
        // die;
        $sql = "EXEC [srv-sql01].[Loyalty].[dbo].[UpdConferee]"
            . "@id_reservation = " . $id_reservation .  ",\n" // — id встречи
            . "@id_employee = '" . $id_employee . "',\n" // — id участника
            . "@del = " . $delete_flag . "\n"  // - 1 если нужно удалить участника"
        ;
        return sql_run($sql);
    }

    // Получение списка конференций
    function get_reservations($date, $room_id = null) {

        $sql = "EXEC  [srv-sql01].[Loyalty].[dbo].[GetConferenceReservation]"
            . "@date = ?," // — дата встреч (формат год.месяц.число - 20191003)
            . "@id_room = ?" // — Если нужны встречи по конкретной переговорке
        ;

        $date = isset($date) ? mktime(0, 0, 0, $date -> {'m'}, $date -> {'d'}, $date -> {'y'}) : time();
        return sql_run($sql, [date('omd', $date), $room_id]);
    }

    if (isset($_POST['query']) && !empty($_POST['query'])) {
            $query = $_POST['query'];
            $result_data = null;

            $adminsIps = ['45.12.220.231', '31.163.29.23'];

            $rooms_template = [
                  'Черника в черном' => 1,
                  'Пельмешки' => 8,
                  'Пломбир' => 9,
                  'Айс' => 10,
                  'Авокадо' => 11,
                  'Манго' => 12,
            ];

            $rooms_orig_ids_template = [
                  '1' => 14,
                  '8' => 9,
                  '9' => 10,
                  '10' => 11,
                  '11' => 12,
                  '12' => 13,
            ];

            switch($query) {
                  case 'getConferenceRooms':
                        $result_data = get_conference_rooms();

                        foreach($result_data as $key => $conference_room) {
                              $name = $conference_room['Name'];
                              if ($rooms_template[$name]) {
                                    $result_data[$key]['id'] = $rooms_template[$name];
                              }
                        }

                        break;

                  case 'getParticipants':
                        $reservationId = strlen($_POST['reservationId']) > 0 ? $_POST['reservationId'] : null;
                        $fio = $_POST['fio'];
                        $result_data = get_participants($reservationId, $fio);
                        break;

                  case 'modifyParticipants':
                        $id_reservation = $_POST['reservationId'];
                        $id_employee = $_POST['code'];
                        $delete_flag = $_POST['status'];
                        $result_data = modify_participants($id_reservation, $id_employee, $delete_flag);

                        // логирование
                        $status = ($delete_flag == 1 ? 'Удален' : 'Добавлен');
                        logger(
                              "<p class='text-info'>$status участник:</p>"
                              . "<span class='text-warning px-2'>ID бронирования:</span> $id_reservation,"
                              . "<span class='text-warning px-2'>ID участника:</span> $id_employee"
                              // . "<span class='text-warning px-2 text-uppercase'>" . ($delete_flag == 1 ? 'удален' : 'добавлен') . "</span>"
                        );
                        // логирование
                        break;

                  case 'getReservations':
                        $date = json_decode($_POST['date']);

                        $conference_rooms = get_conference_rooms();

                        $reservations = array();

                        foreach($conference_rooms as $conference_key => $conference_room) {
                              $conference_room_id = $conference_room['id'];
                              $conference_room_name = $conference_room['Name'];

                              $array_reservations = get_reservations($date, $conference_room_id);

                              if ($rooms_template[$conference_room_name]) {
                                    $conference_room_id = $rooms_template[$conference_room_name]; // меняю id для правильного порядка на фронтенд
                              }

                              // добавляю начальное и конечное время
                              foreach($array_reservations as $key => $reservation) {

                                    $start_time = $reservation['date_from']->format('H:i');
                                    $end_time = $reservation['date_to']->format('H:i');

                                    // записываю все промежутки между начальным и конечным (шаг 30мин)
                                    $times_start_to_end = array();
                                    foreach($time_intervals as $begin_interval) {
                                          if (strnatcasecmp($begin_interval, $start_time) > -1 && strnatcasecmp($begin_interval, $end_time) < 1) {
                                                      // почему то появляется совпадение на 19:00 из-за этого проверяем на уникальность
                                                      if(!in_array($begin_interval, $times_start_to_end, true)) {
                                                            array_push($times_start_to_end, $begin_interval);
                                                      }
                                          }
                                    }

                                    $array_reservations[$key]['times_start_to_end'] = $times_start_to_end;
                                    $array_reservations[$key]['id_room'] = $conference_room_id;
                              }

                              $reservations[$conference_room_id] = $array_reservations;
                        }

                        $result_data = $reservations;
                        break;

                  case 'modifyReservation':
                        $data = array();
                        $room_id = $_POST['conferenceRoomId'];


                        if ($rooms_orig_ids_template[$room_id]) {
                              $room_id = $rooms_orig_ids_template[$room_id];
                        }

                        $time_start = date('H:i', strtotime($_POST['startTime']));
                        $time_end = date('H:i', strtotime($_POST['endTime']));

                        $date = json_decode($_POST['getReservDate']);
                        $reservations = get_reservations($date, $room_id);

                        $time_exist = false;

                        foreach($reservations as $reservation) {
                              $begin_interval = $reservation['date_from']->format('H:i');
                              $end_interval = $reservation['date_to']->format('H:i');

                              if (strnatcasecmp($begin_interval, $time_start) > -1 && strnatcasecmp($begin_interval, $time_end) < 1) {
                                    $time_exist = true;
                              } else if (strnatcasecmp($end_interval, $time_start) > -1 && strnatcasecmp($end_interval, $time_end) < 1) {
                                    $time_exist = true;
                              }

                              if ($time_exist) {
                                    if ($room_id != $reservation['id_room']) {      // нестрогое равенство, т.к. число и строка
                                          echo json_encode([
                                                '$begin_interval' =>  $begin_interval,
                                                '$end_interval' =>  $end_interval,
                                                '$time_start' =>  $time_start,
                                                '$time_end' =>  $time_end,
                                                '$room_id' => $room_id,
                                                '$reservation[id_room]' => $reservation['id_room']
                                          ]);
                                          die;
                                    }
                              }
                        }

                        $data['id_reservation'] = strlen($_POST['reservationId']) > 0 ? $_POST['reservationId'] : 'NULL';
                        $data['datefrom'] = $_POST['date'] . $_POST['startTime'];
                        $data['dateto'] = $_POST['date'] . $_POST['endTime'];
                        $data['id_room'] = $room_id;
                        $data['id_organizer'] = $_POST['nameCode'];
                        $data['description'] = $_POST['description'];
                        $data['name'] = $_POST['title'] ? $_POST['title'] : 'Без названия';
                        $result_data = modify_reservation($data);

                        // логирование
                        $status = ($data['id_reservation'] === 'NULL' ? 'Создание' : 'Изменение');
                        logger(
                              "<p class='text-info'>$status бронирования: <span class='text-light px-2'>" . $data['name'] . "</span></p>     "
                              . "<span class='text-warning px-2'>ID бронирования:</span>  " . ($data['id_reservation'] === 'NULL' ? $result_data[0]['id_reservation'] : $data['id_reservation'])
                              . ", <span class='text-warning px-2'>старт:</span> " . date('d.m.Y | H:i', strtotime($data['datefrom']))
                              . ", <span class='text-warning px-2'>окончание:</span> " . date('d.m.Y | H:i', strtotime($data['dateto']))
                              . ", <span class='text-warning px-2'>переговорная:</span> " . $data['id_room']
                              . ", <span class='text-warning px-2'>организатор:</span> " . $data['id_organizer']
                        );
                        // логирование
                        break;

                  case 'deleteReservation':
                        $id_reservation = $_POST['reservationId'];
                        $result_data = delete_reservation($id_reservation);
                        // логирование
                        logger(
                              "<p class='text-info'>Удаление бронирования:</p>     "
                              . "<span class='text-warning px-2'>ID бронирования:</span>  " . $id_reservation
                        );
                        // логирование
                        break;
                  case 'checkIfAdmin':
                        $ip = getIP();
                        if (in_array($ip, $adminsIps)) {
                              $result_data = true;
                        } else {
                              $result_data = false;
                        }
                        break;
            }

            echo json_encode($result_data);
            exit;
    }

$time_start = 'T21:30:00.000';
$begin_interval = 'T21:30:00.000';
echo '1 === 2 :  ' . strnatcasecmp($begin_interval, $time_start) . '<br />';
$time_start = 'T22:30:00.000';
$begin_interval = 'T21:30:00.000';
echo '1 > 2 :    ' . strnatcasecmp($begin_interval, $time_start) . '<br />';
$time_start = 'T20:30:00.000';
$begin_interval = 'T21:30:00.000';
echo '1 < 2 :      ' . strnatcasecmp($begin_interval, $time_start) . '<br />';


//    print_r(array_unique($aa));

?>
