import React from 'react';
import {createSelector} from 'reselect';
import {getAvailableTimes} from './utils';

export const selectorTimesStart = createSelector(
    ({startTime}) => startTime,
    ({endTime}) =>  endTime,
    ({ownTimes}) =>  ownTimes,
    ({busyTimes}) =>  busyTimes,
    ({pickedDate}) =>  pickedDate,
    (startTime, endTime, ownTimes, busyTimes) =>
        getAvailableTimes('start', {busyTimes, startTime, endTime, ownTimes})
            .map(period => <option value={period} key={period}>{period}</option>)
);
export const selectorTimesEnd = createSelector(
    ({startTime}) => startTime,
    ({endTime}) =>  endTime,
    ({ownTimes}) =>  ownTimes,
    ({busyTimes}) =>  busyTimes,
    ({pickedDate}) =>  pickedDate,
    (startTime, endTime, ownTimes, busyTimes) =>
        // console.log("TCL: startTime, endTime, ownTimes, busyTimes", startTime, endTime, ownTimes, busyTimes) ||
        getAvailableTimes('end', {busyTimes, startTime, endTime, ownTimes})
            .map(period =><option value={period} key={period}>{period}</option>)
);

export const dateCellsSelector = createSelector(
  ({tableDate}) => tableDate,
  ({conferenceRooms}) => conferenceRooms.length,
  (tableDate, quanity) => Array(quanity).fill().map(() => (new Date(tableDate).toLocaleString('ru')).replace(/,\s.+/, ''))
);