import {noop} from './utils';
// для теста, стереть!!!
// import __categories from '../temp_data/categories';
// import __reservations from '../temp_data/reservations';
// import __participants from '../temp_data/participants';
// для теста, стереть!!!


const API_URL = 'https://timepad.vkusvill.ru:40113/serve.php';

const transformObjToFormData = (obj) => {
    const keys = Object.keys(obj);
    if (!keys.length) { return; }
    
    const formData = new FormData();
    Object.keys(obj).forEach(key => {
        const isObj = typeof obj[key] === 'object';
        formData.append(key, isObj ? JSON.stringify(obj[key]) : obj[key]);
    });

    return formData;
}

export const customFetch = ({query, ...options}, cb = noop) =>
    fetch(API_URL, { method: 'POST', body: transformObjToFormData({query, ...options}) })
        .then(response => response.json())
        .then(data => {cb(data); return data;})
        .catch(e => console.error(e));

const getConferenceRooms = (id = '', cb = noop) =>
    customFetch({ query: 'getConferenceRooms', id }, cb);

const getParticipants = ({reservationId = '', fio}, cb) => 
    customFetch({ query: 'getParticipants', reservationId, fio }, cb);

const getReservations = ({date, room_id = ''}, cb) =>
    customFetch({ query: 'getReservations', date: JSON.stringify(date) }, cb);

const modifyReservation = async ({name, nameCode, ...dataObj}, callback) => {
      const [organizator] = nameCode ? [{code: nameCode}] : await getParticipants({fio: name});
      if (!organizator) {
            throw new Error('Отсутствует код клиента для ' + name);
      }

      const [data] = await customFetch({ query: 'modifyReservation', ...dataObj, nameCode: organizator.code }) || [];
      const {id_reservation: reservationId} = data;

      if(!reservationId) {
            throw new Error('Выбранное время занято');
      }

      callback();

      const {participants} = dataObj;
      if(participants.length) {
            for (let participant of participants) {
                  await customFetch({ query: 'modifyParticipants', reservationId, ...participant });
            }
      }
}

const deleteReservation = (reservationId, cb) =>
    customFetch({ query: 'deleteReservation', reservationId }, cb);

const checkIfAdmin = async () =>
    localStorage.getItem('$$#%admi__*&^n')
    ||
    customFetch(
        { query: 'checkIfAdmin' },
        (isAdmin) => isAdmin && localStorage.setItem('$$#%admi__*&^n', isAdmin)
    );


export const api = {
    getParticipants,
    getConferenceRooms,
    getReservations,
    modifyReservation,
    deleteReservation,
    checkIfAdmin
}

{ 
    {/*

    const {separated, dates} = `
    30.10.2019
    Розница Тилси
    (09:30 - 18:00)
    Зарезервировал: Алина Канина 

    Встреча коммуникаций
    (10:00 - 13:00)
    Зарезервировал: Наталья Шувалова 

    Какая-то встреча
    (11:00 - 13:00)
    Зарезервировал: Олег Беляев 


    Тестирование ЛК
    (11:00 - 13:00)
    Зарезервировал: Дарья Богатырева 

    Какая-то встреча
    (12:00 - 13:00)
    Зарезервировал: Анастасия Фокина 

    TrashBack (Алексей Маслов)
    (16:00 - 18:00)
    Зарезервировал: Александр Цыганков 


    ###############


    31.10.2019
    встреча с Томасом и Павлом Смольским по аудиту логистики
    (08:00 - 10:00)
    Зарезервировал: Валерий Разгуляев 

    Встреча по оборудованию
    (14:00 - 16:00)
    Зарезервировал: Анастасия Сазанова 


    Встреча ОС
    (14:00 - 16:00)
    Зарезервировал: Анастасия Сазанова 


    Какая-то встреча
    (18:00 - 20:00)
    Зарезервировал: Марк Измайлов 



    ###############

    01.11.2019
    Встреча проектов с Анастасией Тоток
    (11:30 - 16:00)
    Зарезервировал: Николай Попович 


    ###############

    02.11.2019
    Акселераторство
    (08:00 - 21:00)
    Зарезервировал: Мария Петровская 


    ###############

    05.11.2019
    Встречи Автомакон
    (08:00 - 13:30)
    Зарезервировал: Виктория Шевчук 

    Интеграция доставки в приложение
    (11:00 - 12:00)
    Зарезервировал: Алеся Шереметьева 

    Логистика
    (11:00 - 12:00)
    Зарезервировал: Марк Измайлов 


    Встреча проектов с Анастасией Тоток
    (11:30 - 16:00)
    Зарезервировал: Николай Попович 

    Какая-то встреча
    (12:00 - 14:00)
    Зарезервировал: Оксана Серебрякова 

    Какая-то встреча
    (13:00 - 17:00)
    Зарезервировал: Алеся Шереметьева 

    Какая-то встреча
    (13:30 - 14:30)
    Зарезервировал: Марк Измайлов 


    Встреча по безопасности
    (14:00 - 16:00)
    Зарезервировал: Анастасия Сазанова 


    Встреча по безопасности
    (14:00 - 16:00)
    Зарезервировал: Анастасия Сазанова 


    Встречи Автомакон
    (14:30 - 21:00)
    Зарезервировал: Виктория Шевчук 

    Какая-то встреча
    (17:00 - 18:00)
    Зарезервировал: Денис Алейников 


    Какая-то встреча
    (18:00 - 19:00)
    Зарезервировал: Денис Алейников 



    ###############

    06.11.2019
    Розница Тилси
    (09:30 - 18:30)
    Зарезервировал: Алина Канина 

    Книжный клуб
    (09:30 - 19:00)
    Зарезервировал: Наталья Шувалова 

    Линдстром (Елена Карпова)
    (12:00 - 14:00)
    Зарезервировал: Александр Цыганков 

    ТМ6: Группа 1
    (13:00 - 21:00)
    Зарезервировал: Николай Попович 

    ТМ6: Группа 2
    (13:00 - 21:00)
    Зарезервировал: Николай Попович 


    ###############

    11.11.2019
    Какая-то встреча
    (08:30 - 17:30)
    Зарезервировал: Оксана Серебрякова 

    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Антон Никулин 

    ТилСи
    (09:00 - 18:00)
    Зарезервировал: Алина Канина 

    ТМ7: Группа 1
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 

    ТМ7: Группа 2
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 


    ###############

    12.11.2019
    Встреча проектов с Анастасией Тоток
    (11:30 - 16:00)
    Зарезервировал: Николай Попович 


    ###############

    13.11.2019
    Встреча проектов с Анастасией Тоток
    (11:30 - 16:00)
    Зарезервировал: Николай Попович 


    ###############

    14.11.2019
    Отборы b2b
    (09:00 - 18:00)
    Зарезервировал: Мария Петровская 

    Встреча проектов с Анастасией Тоток
    (11:30 - 16:00)
    Зарезервировал: Николай Попович 


    ###############

    15.11.2019
    Открытая стажировка
    (08:00 - 21:00)
    Зарезервировал: Мария Петровская 


    ###############


    18.11.2019
    Тилси
    (08:30 - 18:30)
    Зарезервировал: Алина Канина 

    Какая-то встреча
    (08:30 - 16:30)
    Зарезервировал: Оксана Серебрякова 

    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Антон Никулин 

    ТМ8: Группа 1
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 

    ТМ8: Группа 2
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 


    ###############

    19.11.2019
    Открытый вторник
    (15:00 - 18:00)
    Зарезервировал: Анастасия Отставных 


    ###############

    25.11.2019
    Какая-то встреча
    (08:30 - 16:30)
    Зарезервировал: Оксана Серебрякова 

    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Антон Никулин 

    Тилси
    (09:30 - 17:30)
    Зарезервировал: Алина Канина 

    ТМ9: Группа 1
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 

    ТМ9: Группа 2
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 


    ###############

    02.12.2019
    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Антон Никулин 

    Тилси
    (09:00 - 18:00)
    Зарезервировал: Алина Канина 

    Какая-то встреча
    (09:00 - 17:00)
    Зарезервировал: Оксана Серебрякова 

    ТМ10: Группа 1
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 

    ТМ10: Группа 2
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 


    ###############

    09.12.2019
    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Антон Никулин 

    Тилси
    (09:00 - 18:00)
    Зарезервировал: Алина Канина 

    Какая-то встреча
    (09:00 - 17:00)
    Зарезервировал: Оксана Серебрякова 

    ТМ11: Группа 1
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 

    ТМ11: Группа 2
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 


    ###############

    16.12.2019
    Какая-то встреча
    (08:30 - 17:30)
    Зарезервировал: Оксана Серебрякова 

    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Антон Никулин 

    Тилси
    (09:00 - 18:00)
    Зарезервировал: Алина Канина 

    ТМ12: Группа 1
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 

    ТМ12: Группа 2
    (11:00 - 19:00)
    Зарезервировал: Николай Попович 


    ###############

    23.12.2019
    Тилси
    (09:00 - 18:00)
    Зарезервировал: Алина Канина 

    Какая-то встреча
    (09:00 - 16:00)
    Зарезервировал: Оксана Серебрякова 

    `.split('###############')
    .reduce((acc, str) => {
        const regexDate = /[\d]+\.[\d]+\.[\d]{4}/;
        const date = str.match(regexDate) && str.match(regexDate)[0];

        //названия встреч
        const regexTitle = /\n[а-я- ]+\n/gi;
        const titles = str.match(regexTitle) && str.match(regexTitle).map(el => el.replace(/\n/g, ''));

        //время
        const regexTime = /\(.+\)/gi;
        const times = str.match(regexTime) && str.match(regexTime).map(el => el.substr(1, 13));

        //Имя
        const regexName = /Зарезервировал: .+\n/gi;
        const names = str.match(regexName) && str.match(regexName).map(el => el.substr(16, (el.length - 16 - 2)));

        const all = titles.map((title, idx) => ({
            title,
            date: date.replace(/(\d+)\.(\d+)\.(\d+)/, "$3-$2-$1"),
            startTime: `T${times[idx] ? times[idx].substr(0,5) : ''}:00.000`,
            endTime: `T${times[idx] ? times[idx].substr(8) : ''}:00.000`,
            conferenceRoomId: 2 + idx,
            participants: [],
            fio: names[idx].replace(/(.+)\s(.+)/, "$2 $1"),
            nameCode: null
        }));

        return {
            separated: acc.separated.concat(all),
            dates: acc.dates.concat(date + ' - ' + all.length)
        };
    }, {separated: [], dates: []});

    // (async() => {
    //     let i = 0;
    //     for(let obj of separated) {
    //         const [participant] = await getParticipants({fio: obj.fio});
    //         if (participant) {
    //             // const data = await modifyReservation({...obj, nameCode: participant.code});
    //         } else {
    //             console.log("TCL: participant - " + obj.date + ' - ' + obj.fio)
    //         }
    //         i++;
    //         // if (i > 0) { break; }
    //     }
    // })();


    
*/}
}
