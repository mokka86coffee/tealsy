export const tableHeaders = Array(16).fill().map((_, idx) => `Переговорная ${idx}`);

export const participants = Array(10).fill()
    .map((_, idx) => ({
        name: `Участник ${idx}`,
        id: idx
    }));

export const notEmptyFormFields = [
    'conferenceRoom',
    'date',
    'startTime',
    'endTime',
    'name'
];

export const keyboardInputFields = [
    'title',
    'name',
    'participantsLabel',
    'description'
];

export const defaultModalState = {
    pickedDate: new Date(),
    startTime: '',
    endTime:  '',
    ownTimes: [],
    name: '',
    nameCode: '',
    title: '',
    conferenceRoomId: '',
    description: '',
    reservationId: '',
    organizators: [],
    participants: [],
    participantsActive: [],

    busyTimesForEachConf: [],
    
    editingMultiSelect: false,

    focusedInputField: null,
    formErrors: {},
    changingConferenceFromProps: {},
    busyTimes: []
}

export const keyboardViews = {
    russian: [
        ['#X#','й','ц','у','к','е','н','г','ш','щ','з','х','ъ','Backspace'],
        ['Caps Lock', 'ф','ы','в','а','п','р','о','л','д','ж','э'],
        ['ENG','я','ч','с','м','и','т','ь','б','ю','?123']
    ],
    english: [
        ['#X#','q','w','e','r','t','y','u','i','o','p','Backspace'],
        ['Caps Lock', 'a','s','d','f','g','h','j','k','l'],
        ['RUS','z','x','c','v','b','n','m','?123']
    ],
    symbols: [
        ['#X#','1','2','3','4','5','6','7','8','9','0','Backspace'],
        ['@','#','_','-','[',']','+','=',':',';','*','&','(', ')'],
        ['RUS','*','"','\'',':',';',',','.','!','?','ENG']
    ]
};

export const keyboardViewsMapper = {
    ENG: 'english',
    RUS: 'russian',
    '?123': 'symbols'
};