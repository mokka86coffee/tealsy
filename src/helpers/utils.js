      import {notEmptyFormFields} from './constants';
      import {api} from './api';

      export const noop = () => {};

      const currentDate = new Date(0,0,0);
      let hour = 8;
      currentDate.setHours(hour);

      export const addZeroToOneDigitString = (digit) => digit / 10 < 1 ? `0${digit}` : `${digit}`;

      export const timePeriods = Array.from({ length: 29 }, (_, idx) => {
            const periodString = `${addZeroToOneDigitString(currentDate.getHours())}:${currentDate.getMinutes() || '00'}`;
            // const periodValue = currentDate.getHours() + (currentDate.getMinutes() / 100);
            const isHoursChange = Boolean(idx % 2) ? true : false;

            if (isHoursChange) {
                  currentDate.setHours(++hour);
                  currentDate.setMinutes(0);
            } else {
                  currentDate.setMinutes(30);
            }

            return periodString;
      });

      export const timeCells = timePeriods.reduce( (acc, periodString, idx) => (
            timePeriods[idx + 1]
                  ? [...acc, periodString + ' - ' +  timePeriods[idx + 1]]
                  : acc
      ), []);

      export const getBusyTimesForEachConf = (dateReservations = {}) =>
      Object.keys(dateReservations).reduce((busyTimes, key) => {
            const busyTimesForConf = dateReservations[key]
                  .map(({times_start_to_end}) => times_start_to_end.slice(0, times_start_to_end.length - 1))
                  .reduce((acc, times) => acc.concat(times), []);

            return {
                  ...busyTimes,
                  [key]: Array.from(new Set(busyTimesForConf)).sort()
            };
      }, {});

      export const getAvailableTimes = (type, {busyTimes = [], startTime, endTime, ownTimes = []}) => {
            const availableTimes = timePeriods.filter(time => ownTimes.slice(0, -1).includes(time) || !busyTimes.includes(time));
            // console.log("TCL: getAvailableTimes -> availableTimes", availableTimes)

            if (!startTime) {
                  return type === 'start'
                        ? availableTimes.slice(0, availableTimes.length - 1)
                        : availableTimes.slice(1);
            } else if (type === 'start') {
                  return availableTimes.slice(0, availableTimes.length - 1);
            }

            const timeIntervalStartIdx = timePeriods.findIndex(time => time === startTime);

            const timeStartIdx = availableTimes.findIndex(time => time === startTime);

            if (timeStartIdx === -1) {
                  return [];
            }

            const endTimes = [];

            // find исп-ся для обрыва выполнения цикла раньше его стандартного завершения
            timePeriods.slice(timeIntervalStartIdx + 1).find((time, idx) => {
                  if (time !== availableTimes[timeStartIdx + idx + 1]) {
                        endTimes.push(time);
                        return true;
                  }
                  endTimes.push(time);
                  return false;
            });

            return endTimes;
      }


      export const timesBookedFromServer = (timesBooked, timeIndex) => {
            const timePeriod = timePeriods[timeIndex];
            const timesBookedKeys = Object.keys(timesBooked);
            timesBookedKeys.sort((a,b) => a-b);

            return timesBookedKeys.map(key => {
                  const conferences = timesBooked[key];

                  if (!conferences.length) {
                        return {timePeriod, conferenceRoomId: key};
                  }

                  const foundedIndex = conferences.findIndex(({times_start_to_end}) => {
                        // т.к. последний элемент является временем окончания встречи
                        const withoutLastOne = times_start_to_end.slice(0,  -1);
                        return withoutLastOne.includes(timePeriod);
                  });

                  if (foundedIndex === -1) {
                        return { timePeriod, conferenceRoomId: key };
                  }

                  const conference = conferences[foundedIndex];

                  return conference.times_start_to_end[0] === timePeriod
                        ? {...conference, way: `${key}/${foundedIndex}`}
                        : undefined;
            });
      }



      export const timeoutToExecute = (time, fn) => {
            let timeout = setTimeout(() => { clearTimeout(timeout); timeout = null; }, time);
            return (...args) => {
                  if (timeout) {
                        fn(...args);
                  }
            }
      }

      export const getDateObjForServer = (stringDate) => {
            const date = new Date(stringDate);

            const day = date.getDate();
            const d = day / 10 < 1 ? `0${day}` : `${day}`;

            const month = date.getMonth() + 1;
            const m = month / 10 < 1 ? `0${month}` : `${month}`;

            const y = date.getFullYear();

            return {d, m, y};
      }

      export const parseFormForServer = async (form) => {
            const dataObj = {}, errors = {};

            for ( let i = 0; i < form.elements.length; i++ ) {
                  const {name, value} = form.elements[i];
                  if (!name) { continue; }

                  if (notEmptyFormFields.includes(name) && !value) {
                        errors[name] = true;
                        continue;
                  }

                  switch(name) {
                        case 'startTime':
                        case 'endTime':
                              dataObj[name] = `T${value}:00.000`;
                              break;

                        case 'conferenceRoomId':
                              dataObj[name] = Number(value);
                              break;

                        case 'date':
                              const [day, month, year] = value.split('.');
                              dataObj.getReservDate = {d: day, m: month, y: year};
                              dataObj[name] = value.replace(/(\d+)\.([\d]+)\.([\d]+)/, "$3-$2-$1");
                              break;

                        case 'zzzzzzz':
                              break;

                        default:
                              dataObj[name] = value;
                  }
            }

            return { dataObj, errors };
      }

      export const excludeErrorField = (name, formErrors) => {
            const {[name]: unusedName, ...updatedFormErrors} = formErrors;
            return updatedFormErrors;
      }

      export const isEqual = (obj1, obj2) => JSON.stringify(obj1) === JSON.stringify(obj2);

      export const transformConferenceServerToState = (serverConference) => {
            const {
                  date_from: { date },
                  times_start_to_end,
                  FIO: name,
                  name_conf: title,
                  id_room: conferenceRoomId,
                  Description: description,
                  id_reservation: reservationId
            } = serverConference;

            const startTime = times_start_to_end[0];
            const endTime = times_start_to_end[times_start_to_end.length - 1];

            return {
                  pickedDate: new Date(date),
                  startTime,
                  endTime,
                  multiSelect: [],
                  ownTimes: times_start_to_end,
                  name,
                  title,
                  conferenceRoomId,
                  description,
                  reservationId
            }
      }

      export const isMobile = () => /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);
