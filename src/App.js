import React, { Component } from 'react';
import {connect} from 'react-redux';
import { DatePickerInput } from 'rc-datepicker';
import 'rc-datepicker/lib/style.css';
import { api } from './helpers/api';
import { Keyboard } from './components';
import Modal from './Modal';
import {
setAskTooltipPosition
} from './store/actions';
import {
      timeCells,
      timesBookedFromServer,
      getDateObjForServer,
      transformConferenceServerToState,
      getBusyTimesForEachConf,
      noop,
      isMobile
} from './helpers/utils';
import { dateCellsSelector } from './helpers/selectors';
import './styles/App.css';

const today = new Date();
const mobileVersion = isMobile();
class App extends Component {
      constructor(props) {
            super(props);
            this.touchTimeout = null;
            this.loaderTimeout = null;
      }

      state = {
            isFullScreenLoaderVisible: true,

            isTouchScreen: undefined,

            isAdmin: false,

            currentDate: today,
            tableDate: today,
            changingConference: {},
            busyTimesForEachConf: {},

            isModalVisible: false,
            loading: true,
            conferenceRooms: [],
            dateReservations: [],

            showAskModal: false,
            askModalX: 0,
            askModalY: 0,

            showKeyboard: false,
            keyboardTopPosition: 100,
            fromKeyboard: ''
      }

      async componentDidMount() {
      const isAdmin = await api.checkIfAdmin();
      const conferenceRooms = await api.getConferenceRooms();
      const conferenceRoomsSorted = conferenceRooms.sort(({id: id1}, {id: id2}) => id1 - id2);

      this.submitDateChange();

      this.setState({
            conferenceRooms: conferenceRoomsSorted,
            loading: false,
            isAdmin
      });

      }

      handleFullScreenLoader = ({nativeEvent: {type}}) => {
            const clearCallback = () => {
                  this.setState({ isFullScreenLoaderVisible: true });
                  clearTimeout(this.loaderTimeout);
                  this.loaderTimeout = null;
                  this.handleModalShow();
                  this.handleKeyboardShow({ visible: false });
                  this.updateChangingConference({});
            }

            if (this.loaderTimeout) {
                  clearTimeout(this.loaderTimeout);
                  this.loaderTimeout = null;
                  this.loaderTimeout = setTimeout(clearCallback, 6 * 1e4);
                  return;
            }

            this.loaderTimeout = setTimeout(clearCallback, 6 * 1e4);
            this.changeCurrentDate(new Date());
            this.setState(({isTouchScreen}) =>
                  ({
                  isFullScreenLoaderVisible: false,
                  ...(isTouchScreen !== undefined) ? { isTouchScreen } : { isTouchScreen: type === 'touchstart' ? true : false }
                  }));
      }

      handleLoading = (loading) => this.setState({ loading })

      changeCurrentDate = (currentDate) => {
            this.setState({currentDate}, this.submitDateChange);
      }

      submitDateChange = async () => {
            this.setState({loading: true, changingConference: {}});

            const {currentDate} = this.state;
            const date = getDateObjForServer(currentDate);

            const dateReservations = await api.getReservations({date});

            const busyTimesForEachConf = getBusyTimesForEachConf(dateReservations);

            this.setState({
                  loading: false,
                  tableDate: currentDate,
                  dateReservations,
                  busyTimesForEachConf,
                  changingConference: { busyTimesForEachConf, pickedDate: currentDate }
            });
      }

      handleKeyboardShow = ({visible, top}) => this.setState({showKeyboard: visible, keyboardTopPosition: top});

      handleKeyboardInput = (fromKeyboard) => {
      this.setState({ fromKeyboard });
      }

      handleModalShow = (status = false, callback = noop) => {
      window.scrollBy(-1e5, -1e5);
      this.setState(({isModalVisible: status}), callback);
      }

      renderAskTooltip = () => {
            const {showAskModal, askModalX, askModalY} = this.state;
            const className = showAskModal ? "askModal" : "askModal askModal--hidden";
            const mobileClassName = mobileVersion ? 'askModal--mobile' : '';
            const style = {top: `${askModalY}px`,left: `${askModalX}px`};
            return (
                  <div style={style} className={`${className} ${mobileClassName}`}>
                        <button
                              className="tealsy-btn"
                              onClick={() => this.setState({isModalVisible: true, showAskModal: false})}
                        >
                              Забронировать
                        </button>
                  <button
                  className="tealsy-btn"
                  onClick={() => this.setState({showAskModal: false})}
                  >
                  Отменить
                  </button>
                  </div>
            )
      }

      renderLoader = () => (
      <div className="table-loader">Загрузка...</div>
      )

      showAskModal = () => this.setState({ showAskModal: true })

      updateChangingConference = (changingConference) => this.setState({ changingConference });

      renderBookedCells = (timeIndex) => {

            const onTouchStart = ({nativeEvent}) => {

                  const {target, touches = []} = nativeEvent;

                  const {conference: way, conferenceChangeType} = target.dataset;
                  const {dateReservations, busyTimesForEachConf, tableDate: pickedDate} = this.state;

                  if (conferenceChangeType === 'new') {
                  const {screenX, screenY, pageX, pageY} = touches[0] || nativeEvent; // для тача или клика мыши
                  console.log("TCL: touches", touches[0])
                  const {conferenceRoomId, conferenceTimePeriod: startTime} = target.dataset;

                  this.touchTimeout = setTimeout(() => (
                        this.props.handleAskTooltip(touches),
                        this.setState({
                              showAskModal: true,
                              askModalX: mobileVersion ? screenX - 200 : pageX,
                              askModalY: mobileVersion ? screenY - 100 : pageY,
                              changingConference: { conferenceRoomId, busyTimesForEachConf, startTime, pickedDate, busyTimes: busyTimesForEachConf[conferenceRoomId] }
                        })
                  ), 300);

                  return;
                  }


                  const [conferenceRoomId, index] = way.split('/');

                  const serverConference = dateReservations[conferenceRoomId][+index];
                  const changingConference = transformConferenceServerToState(serverConference);

                  if (conferenceChangeType === 'delete') {
                        api.deleteReservation(changingConference.reservationId, this.submitDateChange);
                        return;
                  }

                  this.setState({
                        changingConference: {...changingConference, busyTimesForEachConf, busyTimes: busyTimesForEachConf[conferenceRoomId]},
                        isModalVisible: true
                  });

            }

            const clearTouchTimeout = () => {
                  if (this.touchTimeout) {
                        clearTimeout(this.touchTimeout);
                        this.setState({ showAskModal: false });
                  }
            }

            const {dateReservations, isAdmin} = this.state;

            return timesBookedFromServer(dateReservations, timeIndex).map((conference,idx) => {
                  if (!conference) {
                        return null;
                  }

                  if (!conference.id_reservation) {
                        return (
                              <td
                                    onTouchStart={onTouchStart}
                                    onTouchMove={clearTouchTimeout}
                                    onDoubleClick={onTouchStart}
                                    data-conference-change-type="new"
                                    data-conference-room-id={conference.conferenceRoomId}
                                    data-conference-time-period={conference.timePeriod}
                                    key={idx}
                              />
                        );
                  }

                  const {
                        way,
                        times_start_to_end,
                        id_reservation,
                        name_conf,
                        FIO
                  } = conference;

                  const timesLength = times_start_to_end.length;

                  return conference
                  ? (
                        <td
                              rowSpan={timesLength - 1}
                              key={id_reservation}
                              onTouchMove={clearTouchTimeout}
                              className="occupied"
                        >
                              <h4>{name_conf}</h4>
                              <p className="timecell__time">{`${times_start_to_end[0]} - ${times_start_to_end[timesLength - 1]}`}</p>
                              <h4>Зарезервировал: {FIO}</h4>
                              {isAdmin && (
                                    <React.Fragment>
                                          <button onClick={onTouchStart} data-conference={way} data-conference-change-type="change"  className="tealsy-btn">Изменить</button>
                                          <button onClick={onTouchStart} data-conference={way} data-conference-change-type="delete" className="tealsy-btn">Освободить</button>
                                    </React.Fragment>
                              )}
                        </td>
                  )
                  : null
            });
      }

      renderTable = () => {
            const {conferenceRooms} = this.state;
            const dateCells = dateCellsSelector(this.state);

            return (
                  <table>
                        <thead>
                              <tr>
                                    <th></th>
                                    {conferenceRooms.map(({id, Name, description}) =>
                                    <th className={`confCard confCard-${id}`} key={id}>
                                          <h5 className="confCard__number">{id}</h5>
                                          <p className="confCard__name">{Name}</p>
                                          <p className="confCard__title">{description}</p>
                                    </th>
                                    )}
                              </tr>
                        </thead>
                        <tbody>
                              {/* Строка с выбранной датой в ячейках */}
                              <tr>
                                    <td>Время</td>
                                    {dateCells.map((date, idx) => <td className="timecell__date" key={idx}>{date}</td> )}
                              </tr>

                              {/* Остальные строки */}
                              {timeCells.map((label, idx) => (
                                    <tr key={label} >
                                          <td className="timecell__time">{label}</td>
                                          {this.renderBookedCells(idx)}
                                    </tr>
                              ))}
                        </tbody>
                  </table>
            );
      }

      render() {
            const {
                  isFullScreenLoaderVisible,
                  currentDate,
                  isModalVisible,
                  loading,
                  showKeyboard,
                  fromKeyboard,
                  keyboardTopPosition,
                  conferenceRooms,
                  changingConference,
                  isTouchScreen } = this.state;

            if (isFullScreenLoaderVisible) {
                  return (
                        <div className="App" onClick={this.handleFullScreenLoader} onTouchStart={this.handleFullScreenLoader}>
                              <div className="center">
                                    <h1>Нажмите на экран для входа</h1>
                                    <div className="loader"></div>
                              </div>
                        </div>
                  );
            }

            return (
                  <div className={`App ${mobileVersion ? 'App--mobile' : ''}`} onClick={this.handleFullScreenLoader} onTouchStart={this.handleFullScreenLoader}>
                        <header className="App-header">
                              <h2>График переговорной</h2>
                              <div>
                                    <button
                                          onClick={() => this.handleModalShow(true)}
                                          className="tealsy-btn"
                                    >
                                          Забронировать переговорную
                                    </button>
                                    <DatePickerInput
                                          value={currentDate}
                                          minDate={new Date()}
                                          locale='ru'
                                          className='calendar-input'
                                          onChange={this.changeCurrentDate}
                                    />
                                    {/* <button onClick={this.submitDateChange} className='tealsy-btn'>Установить</button> */}
                              </div>
                        </header>
                        {loading ? this.renderLoader() : this.renderTable()}
                        <Modal
                              handleModalShow={this.handleModalShow}
                              isModalVisible={isModalVisible}
                              refreshTable={this.submitDateChange}
                              handleKeyboardShow={this.handleKeyboardShow}
                              handleKeyboardInput={this.handleKeyboardInput}
                              isKeyboardVisible={showKeyboard}
                              keyboardInput={fromKeyboard}
                              conferenceRooms={conferenceRooms}
                              changingConference={changingConference}
                              updateChangingConference={this.updateChangingConference}
                              isTouchScreen={isTouchScreen}
                        />
                        {this.renderAskTooltip()}
                        <Keyboard
                              visible={showKeyboard}
                              keyboardTopPosition={keyboardTopPosition}
                              handleKeyboardShow={this.handleKeyboardShow}
                              getInput={this.handleKeyboardInput}
                              input={fromKeyboard}
                        />
                        </div>
                  );
            }
}

export default connect(
null,
dispatch => ({
handleAskTooltip: e => {
      dispatch(setAskTooltipPosition(e))
}
}))(App);
