import React from 'react';
import './style.css';

import {connect} from 'react-redux';

const AskTooltip = ({showAskModal, askModalX, askModalY}) => {
    // const {showAskModal, askModalX, askModalY} = this.state;
    const className = showAskModal ? "askModal" : "askModal askModal--hidden";
    const style = {top: `${askModalY}px`,left: `${askModalX}px`};
    return (
      <div style={style} className={className}>
        <button
          className="tealsy-btn"
          onClick={() => this.setState({isModalVisible: true, showAskModal: false})}
        >
          Забронировать
        </button>
        <button
          className="tealsy-btn"
          onClick={() => this.setState({showAskModal: false})}
        >
          Отменить
        </button>
      </div>
    )
}

export default connect(
    store => store,
    null
)(AskTooltip);