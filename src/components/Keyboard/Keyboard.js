import React, {PureComponent} from 'react';
import {createSelector} from 'reselect';
import {keyboardViews, keyboardViewsMapper} from '../../helpers/constants';
import './style.css';

const keyboardViewSelector = createSelector(
    ({view}) => view,
    view => keyboardViews[view]
);

export default class Keyboard extends PureComponent {

    state = {
        capsLock: false,
        view: 'russian'
    }

    handleInput = ({target: {value: key}}) => {
        console.log("TCL: Keyboard -> handleInput -> key", key)
        if (['?123', 'RUS', 'ENG'].includes(key)) {
            this.setState({view: keyboardViewsMapper[key]});
            return;
        }

        const {getInput, input: prevInput} = this.props;

        if (key ==='Caps Lock') {
            this.setState(({capsLock}) => ({capsLock: !capsLock}));
            return;
        }

        const updatedInput = key === 'Backspace' ? prevInput.slice(0, (prevInput.length - 1)) : prevInput + key; 
        getInput(updatedInput);
    }

    renderKeys = (keys) => {
        const {capsLock} = this.state;
        return keys.map(key => {
            const withCapsKey = (key.length > 1) ? key : capsLock ? key.toUpperCase() : key;
            return  (
                <button
                    className='keyboard_key'
                    value={withCapsKey}
                    key={withCapsKey}
                    onClick={this.handleInput}
                >
                    {withCapsKey === '#X#' ? 'X' : withCapsKey}
                </button>
            );
        });
    }

    render() {
        const {visible, keyboardTopPosition} = this.props;
        const keyboard = keyboardViewSelector(this.state);
        return (
            // <div style={{top: keyboardTopPosition}} className={visible ? "keyboard" : "keyboard keyboard--hidden"}>
            <div className={visible ? "keyboard" : "keyboard keyboard--hidden"}>
                {keyboard.map((keysRow, idx) => (
                    <div key={idx} className={idx === 0 ? 'keyboard__keys keyboard__keys--first-row' : ''} >
                        {this.renderKeys(keysRow)}   
                    </div>
                ))}
                <div>
                    <button value=" " className='keyboard__key keyboard_key--space' onClick={this.handleInput}>пробел</button>
                </div>
            </div>
        );
    }
}