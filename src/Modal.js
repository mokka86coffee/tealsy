import React from 'react';
import { DatePickerInput } from 'rc-datepicker';
import { api } from './helpers/api';
import {
    parseFormForServer,
    excludeErrorField,
    isEqual,
    getBusyTimesForEachConf,
    getDateObjForServer,
    isMobile
} from './helpers/utils';
import {defaultModalState, keyboardInputFields} from './helpers/constants';
import {selectorTimesStart, selectorTimesEnd} from './helpers/selectors';
import './styles/Modal.css';


export default class Modal extends React.PureComponent {
    constructor(props) {
        super(props);
        this.nameRef = React.createRef();
        this.titleRef = React.createRef();
        this.participantsRef = React.createRef();
        this.descriptionRef = React.createRef();
    }

    state = { ...defaultModalState };

    async componentDidUpdate({keyboardInput: prevKeyboardInput, changingConference: {reservationId: prevReservationId}, isModalVisible: prevIsModalVisible}) {
        const {keyboardInput: newKeyboardInput, isModalVisible, handleKeyboardInput, handleKeyboardShow, changingConference: {reservationId: newReservationId} } = this.props;
        const {focusedInputField, formErrors, pickedDate} = this.state;
        if (!isModalVisible && (isModalVisible !== prevIsModalVisible)) {
            this.resetModalForm();
        }

        if (newReservationId !== prevReservationId) {
            const participantsAdded = await api.getParticipants({reservationId: newReservationId});
            const participantsActive = participantsAdded.map(({id_employee, ...rest}) => ({...rest, code: id_employee, prevStatus: 0, status: 0}));
            this.setState({participantsActive});
        }

        if (!focusedInputField) {
            return;
        }

        if (newKeyboardInput !== prevKeyboardInput) {

            const clearedType = newKeyboardInput.indexOf('#X#');
            if (clearedType > -1) {
                this.setState({
                    focusedInputField: null,
                    formErrors: excludeErrorField(focusedInputField, formErrors),
                    [focusedInputField]: focusedInputField === 'participantsLabel' ? '' : prevKeyboardInput
                }, () => {
                        handleKeyboardInput('');
                        handleKeyboardShow({visible: false});
                    }
                );

                return;
            }

            if (['name', 'participantsLabel'].includes(focusedInputField)) {
                const field = focusedInputField === 'name' ? 'organizators' : 'participants';
                if (newKeyboardInput.length > 3) {
                    const participants = await api.getParticipants({fio: newKeyboardInput});
                    this.setState({[field]: participants});
                } else {
                    this.setState({[field]: []});
                }
            }

            this.setState({ [focusedInputField]: newKeyboardInput });
        }


    }

    static getDerivedStateFromProps({changingConference}, {changingConferenceFromProps, ...rest}) {
        if (!isEqual(changingConference, changingConferenceFromProps)) {
            return Object.keys(changingConference).length
                ? {...changingConference, changingConferenceFromProps: changingConference}
                : {defaultModalState};
        }
        return null;
    }

    getBusyAndOwnTimes = async (pickedDate) => {
        const {reservationId, conferenceRoomId} = this.state;

        const date = getDateObjForServer(pickedDate);
        const dateReservations = await api.getReservations({date});

        const {times_start_to_end: ownTimes} = dateReservations[conferenceRoomId].find(({id_reservation}) => id_reservation === reservationId) || {};

        const busyTimesForEachConf = getBusyTimesForEachConf(dateReservations);

        return {busyTimesForEachConf, ownTimes};
    }

    pickDate = async (pickedDate) => {
        const {conferenceRoomId} = this.state;
        const {busyTimesForEachConf, ownTimes} = await this.getBusyAndOwnTimes(pickedDate);

        this.setState({
            pickedDate,
            busyTimes: busyTimesForEachConf[conferenceRoomId],
            busyTimesForEachConf,
            ownTimes
        });
    }

    handleCustomSelect = ({target: {dataset: {value, type, label}}, nativeEvent}) => {
        nativeEvent.stopPropagation();
        nativeEvent.preventDefault();

        this.setState(
            {name: label, nameCode: value, focusedInputField: null, formErrors: {}},
            () => {
                document.activeElement.blur();
                this.props.handleKeyboardInput('#X#');
            }
        );
    }

    handleMultiSelectAdd = ({target: {dataset: {value, type, label}}, nativeEvent}) => {
        const founded = this.state.participantsActive.find(({code}) => code === value);
        if (founded) { return; }

        nativeEvent.stopPropagation();
        nativeEvent.preventDefault();

        const participantsActive = this.state.participantsActive.concat({FIO: label, code: value, status: 0});
        this.setState({participantsActive, participants: []}, () => this.props.handleKeyboardInput('#X#'));
    }

    handleMultiSelectDelete = ({target: {dataset: {id}}, nativeEvent: event}) => {
        const {participantsActive} = this.state;

        const updatedParticipants = participantsActive.map(participant => id === participant.code ? {...participant, status: 1} : participant );

        event.preventDefault();
        event.stopPropagation();

        this.setState({participantsActive: updatedParticipants});
    }

    handleTimeChange = ({target: { value: changedTime }, currentTarget: { name }}) => {
        const {formErrors} = this.state;
        if (name === 'startTime') {
            this.setState({startTime: changedTime});
        } else {
            this.setState({endTime: changedTime});
        }

        if(changedTime && (name in formErrors)) {
            this.setState({ focusedInputField: null, formErrors: excludeErrorField(name, formErrors) });
        }
    }

    handleInput = ({target}) => {
        const {name, value} = target;
        const {isTouchScreen} = this.props;

        this.setState({ focusedInputField: name });

        if (!isTouchScreen || isMobile()) {
            target.focus();
            return;
        }

        target.readOnly = true;
        const position = target.getBoundingClientRect();
        const keyboardTopPoint = window.innerHeight - 430; // верхняя граница клавиатуры
        if ((position.top + 200) >= keyboardTopPoint) {
            const visiblePoint = position.top + position.height - keyboardTopPoint + 200;
            this.modal.scrollBy(0, visiblePoint);
        }

        const {handleKeyboardInput, handleKeyboardShow} = this.props;

        handleKeyboardInput(value);

        handleKeyboardShow({visible: true, top: window.innerHeight - (position.top + position.height)});
    }

    hideListsOpened = () => this.setState({focusedInputField: null});

    hideKeyboard = ({target: {name}}) => {
        const {handleKeyboardShow, isKeyboardVisible} = this.props;
        if (!keyboardInputFields.includes(name) && isKeyboardVisible) {
            handleKeyboardShow({visible: false});
        }
    }

    handleConferenceRoomIdChange = async ({target: {value}} = {target: {value: ''}}) => {
        const {formErrors} = this.state;
        const {
            busyTimesForEachConf,
            changingConferenceFromProps: {conferenceRoomId: originalRoomId}
        } = this.state;

        if(value && (name in formErrors)) {
            this.setState({ focusedInputField: null, [name]: value, formErrors: excludeErrorField(name, formErrors) });
            return;
        }

        const backToOriginal = +value === originalRoomId;

        this.setState({
            conferenceRoomId: +value,
            busyTimes: busyTimesForEachConf[value],
            ...backToOriginal ? {} : {ownTimes: []}
        }, async () => {
            if (backToOriginal) {
                const {ownTimes} = await this.getBusyAndOwnTimes(this.state.pickedDate);
                this.setState({ownTimes});
            }
        });
    }

    handleRealKeyboardInput = ({target: {value}}) => {
        this.props.handleKeyboardInput(value);
    }

    resetFocusedField = () => this.setState({focusedInputField: null});

    resetModalForm = () => {
        setTimeout(() => {
            this.modal.scrollTo(0,0);
            this.setState(defaultModalState);
        }, 300);
    }

    handleSubmit = async ({nativeEvent}) => {
        nativeEvent.preventDefault();

        const form = nativeEvent.target;
        const { errors, dataObj } = await parseFormForServer(form);

        if (Object.keys(errors).length) {
            this.setState({formErrors: errors});
            return;
        }

        const {
            handleModalShow,
            refreshTable,
            updateChangingConference,
            handleKeyboardShow
        } = this.props;

        const {
            reservationId,
            participantsActive: participants
        } = this.state;

        const callback = () => {
            refreshTable();
            updateChangingConference({});
            handleKeyboardShow({ visible: false });
            handleModalShow();
        }

        // если время занято, появится ошибка
        try {
            await api.modifyReservation({
                ...dataObj,
                reservationId,
                participants: participants.filter(({prevStatus, status}) => status !== prevStatus)
            }, callback);
        } catch (err) {
            this.handleConferenceRoomIdChange();
            alert(err.message);
            this.modal.scrollTo(0,0);
        }

    }

    handleCancel = () => {
        const {handleModalShow, updateChangingConference, handleKeyboardShow} = this.props;
        updateChangingConference({});
        handleKeyboardShow({ visible: false });
        handleModalShow();
    }

    render() {
        const {
            isModalVisible,
            conferenceRooms
        } = this.props;

        const {
            pickedDate,
            editingMultiSelect,
            startTime,
            endTime,
            name,
            nameCode,
            participantsLabel,
            participantsCode,
            title,
            conferenceRoomId,
            description,
            focusedInputField,
            formErrors,
            organizators,
            participants,
            participantsActive: participantsActiveUnfiltered
        } = this.state;

        const participantsActive = participantsActiveUnfiltered.filter(({status}) => status === 0);

        return (
            <div
                ref={ref => this.modal = ref}
                onTouchStart={() => { this.hideKeyboard(); this.hideListsOpened(); }}
                className={isModalVisible ? 'modal__wrap' : 'modal__wrap modal__wrap--hidden'}
            >
                <div className="App-header">
                    <h2>Резервирование переговорной</h2>
                    <div>
                        <button onClick={this.handleCancel} className="tealsy-btn btn--top-left">Назад</button>
                    </div>
                </div>
                <form onSubmit={this.handleSubmit} className="modal__form">
                    <label>
                        <span>Переговорная</span>
                        <select
                            className={"conferenceRoomId" in formErrors ? "modal__select modal__select--error" : "modal__select"}
                            name="conferenceRoomId"
                            onChange={this.handleConferenceRoomIdChange}
                            value={conferenceRoomId}
                        >
                            <option value=""></option>
                            {conferenceRooms.map(({id, Name}) => <option value={id} key={id}>{Name}</option>)}
                        </select>
                    </label>
                    <label>
                        <span>Дата</span>
                        <DatePickerInput
                            value={pickedDate}
                            name="date"
                            minDate={new Date()}
                            locale='ru'
                            className='modal__input--calendar'
                            onChange={this.pickDate}
                            // {...anyReactInputProps}
                        />
                    </label>
                    <label>
                        <span>Время начала</span>
                        <select
                            className={"startTime" in formErrors ? "modal__select modal__select--error" : "modal__select"}
                            name="startTime"
                            onChange={this.handleTimeChange}
                            value={startTime}
                        >
                            <option value=""></option>
                            {selectorTimesStart(this.state)}
                        </select>
                    </label>
                    <label>
                        <span>Время окончания</span>
                        <select
                            className={"endTime" in formErrors ? "modal__select modal__select--error" : "modal__select"}
                            name="endTime"
                            onChange={this.handleTimeChange}
                            value={endTime}
                        >
                            <option value=""></option>
                            {selectorTimesEnd(this.state)}
                        </select>
                    </label>
                    <label>
                        <span>Название</span>
                        <input
                            className={"title" in formErrors ? "modal__input modal__input--error" : "modal__input"}
                            onFocus={this.handleInput}
                            value={title}
                            name="title"
                            type="text"
                            ref={this.titleRef}
                            onChange={this.handleRealKeyboardInput}
                        />
                    </label>
                    <label>
                        <span>Инициатор</span>
                        <input
                            onFocus={this.handleInput}
                            value={name}
                            className={"name" in formErrors ? "modal__input modal__input--error" : "modal__input"}
                            name="name"
                            type="text"
                            ref={this.nameRef}
                            onChange={this.handleRealKeyboardInput}
                        />
                        <input
                            readOnly
                            value={nameCode}
                            name="nameCode"
                            type="hidden"
                        />
                        <div
                            className={`
                                custom-select
                                ${focusedInputField === 'name' ? "" : "custom-select--hidden"}
                                ${organizators.length > 0 ? 'custom-select--bordered' : ''}
                            `}
                        >
                            {organizators.map(({FIO, code}) => <p key={code} data-type="name" data-label={FIO} data-value={code} onClick={this.handleCustomSelect}>{FIO}</p>)}
                        </div>
                    </label>
                    <label>
                        <span>Участники</span>
                        <input
                            onFocus={this.handleInput}
                            value={participantsLabel}
                            className="modal__input"
                            name="participantsLabel"
                            type="text"
                            ref={this.participantsRef}
                            onChange={this.handleRealKeyboardInput}
                        />
                        <input
                            readOnly
                            value={participantsCode}
                            name="participantsCode"
                            type="hidden"
                        />
                        {participantsActive.length > 0 && (
                            <div className={editingMultiSelect ? "modal__select-overlay modal__select-overlay--editing" : "modal__select-overlay"}>
                                {participantsActive.map(({FIO, code, status}) => (
                                    <span key={code}>
                                        {FIO}
                                        <button type="button" onClick={this.handleMultiSelectDelete} data-id={code} className="modal__select-overlay__delete-btn">x</button>
                                    </span>
                                ))}
                            </div>
                        )}
                        <div className={focusedInputField === 'participantsLabel' ? "custom-select" : "custom-select custom-select--hidden"}>
                            {participants.map(({FIO, code}) => <p key={code} data-type="name" data-label={FIO} data-value={code} onClick={this.handleMultiSelectAdd}>{FIO}</p>)}
                        </div>
                        <button
                            onClick={() => this.setState(state => ({editingMultiSelect: !state.editingMultiSelect}))}
                            className="tealsy-btn btn-edit"
                            type="button"
                        >
                            {editingMultiSelect ? 'Сохранить' : 'Изменить'}
                        </button>
                    </label>
                    <label>
                        <span>Описание</span>
                        <textarea
                            value={description}
                            onFocus={this.handleInput}
                            className="modal__textarea"
                            name="description"
                            ref={this.descriptionRef}
                            onChange={this.handleRealKeyboardInput}
                        />
                    </label>
                    <button type="submit" className="tealsy-btn btn-bottom">Забронировать</button>
                </form>
            </div>
        )
    }
}
