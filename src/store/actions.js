export const SET_ASK_TOOLTIP_POSITION = 'SET_ASK_TOOLTIP_POSITION';
export const setAskTooltipPosition = (...rest) => ({type: SET_ASK_TOOLTIP_POSITION, payload: {rest}});