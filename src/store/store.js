import {createStore, combineReducers} from 'redux';
import {SET_ASK_TOOLTIP_POSITION} from './actions';

const askTooltipReducer = (store = {}, {type, payload}) => {
    switch (type) {
        case SET_ASK_TOOLTIP_POSITION: return {...store, ...payload}
    }
};

export const store = createStore(askTooltipReducer, {}, window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__());

store.dispatch({type: 'add'});